# Run from within a conda environment from environment.yml

IGUIDEBIN = $(CONDA_PREFIX)/bin/iguide
GRABSEQSBIN = $(CONDA_PREFIX)/bin/grabseqs
IGUIDE_DIR ?= $(PWD)/iGUIDE
BIOPROJ ?= PRJNA506241

RAW_R1 = $(wildcard raw/*_1.fastq.gz)
RAW_R2 = $(wildcard raw/*_2.fastq.gz)
RAW_I1 = $(patsubst raw/%_1.fastq.gz, raw/%_I1.fastq.gz, $(RAW_R1))
RAW_I2 = $(patsubst raw/%_2.fastq.gz, raw/%_I2.fastq.gz, $(RAW_R2))

IGUIDE_METADATA = $(addprefix iGUIDE/sampleInfo/example., sampleInfo.csv supp.csv)
IGUIDE_DATA = $(addsuffix .fastq.gz, $(addprefix data/, R1 R2 I1 I2))
IGUIDE_REPORT = iGUIDE/analysis/example/reports/report.example.html

all: $(IGUIDE_REPORT)

data: $(IGUIDE_DATA)
metadata: $(IGUIDE_METADATA)

$(IGUIDE_REPORT): $(IGUIDEBIN) iGUIDE/analysis/example/config.yml $(IGUIDE_METADATA) $(IGUIDE_DATA)
	IGUIDE_DIR=$(IGUIDE_DIR) $< run $(word 2, $^) -- --jobs 16 --notemp -rp

# using metadata as make target but also implicitly gets the data files
raw/metadata.csv: $(GRABSEQSBIN)
	$< sra -m $(notdir $@) -o $(dir $@) $(BIOPROJ)

# Making grabseqs setup dependent on iguide because we're using a strict list
# of package for iguide (if grabseqs is installed first it'll then get removed)
$(GRABSEQSBIN): $(IGUIDEBIN)
	conda env update --file environment.yml

raw/%_I1.fastq.gz: raw/%_1.fastq.gz
	bash scripts/fake_index.sh $< > $@

raw/%_I2.fastq.gz: raw/%_2.fastq.gz
	bash scripts/fake_index.sh $< > $@

data/R1.fastq.gz: raw/metadata.csv
	mkdir -p $(dir $@)
	cat raw/*_1.fastq.gz > $@
data/R2.fastq.gz: raw/metadata.csv
	mkdir -p $(dir $@)
	cat raw/*_2.fastq.gz > $@
data/I1.fastq.gz: raw/metadata.csv $(RAW_I1)
	mkdir -p $(dir $@)
	cat raw/*_I1.fastq.gz > $@
data/I2.fastq.gz: raw/metadata.csv $(RAW_I2)
	mkdir -p $(dir $@)
	cat raw/*_I2.fastq.gz > $@

# Install iGUIDE into the existing environment
$(IGUIDEBIN): iGUIDE/install.sh
	cd $(dir $<) && ./$(notdir $<) -e $(notdir $(CONDA_PREFIX)) --update all

# SraRunTable.tsv generated from:
# https://trace.ncbi.nlm.nih.gov/Traces/study/?acc=PRJNA506241

# The per-sequencing-sample information (like per SRR)
iGUIDE/sampleInfo/example.sampleInfo.csv: SraRunTable.tsv raw/metadata.csv $(RAW_I1) $(RAW_I2)
	Rscript scripts/prep_sample_info.R $< $@

# The per-specimen info (like, per BioSample)
iGUIDE/sampleInfo/example.supp.csv: SraRunTable.tsv
	Rscript scripts/prep_sample_supp.R $< $@

iGUIDE/analysis/example/config.yml: $(IGUIDEBIN) iGUIDE/configs/example.config.yml
	IGUIDE_DIR=$(IGUIDE_DIR) $< setup $(word 2, $^)

iGUIDE/configs/%.yml: %.yml
	cp $< $@
