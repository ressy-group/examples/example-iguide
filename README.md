# iGUIDE Example

A reproduction of the integration site results from the [iGUIDE Paper] starting
from the raw data from [PRJNA506241] and processing with [iGUIDE] as per the
paper and [documentation].

    git clone --recursive git@github.com:ressy/example-iguide.git
    cd example-iguide
    conda env create --name example-iguide
    conda activate example-iguide
    make

[iGUIDE]: https://github.com/cnobles/iGUIDE
[iGUIDE Paper]: https://github.com/cnobles/iGUIDE_manuscript_analysis
[PRJNA506241]:  https://www.ncbi.nlm.nih.gov/bioproject/PRJNA506241
[documentation]: https://iguide.readthedocs.io
