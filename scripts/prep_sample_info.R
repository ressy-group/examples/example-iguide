# Use an SRA run info table to create the iGUIDE sample info table.
source("scripts/helpers.R")
args <- commandArgs(trailingOnly = TRUE)
sample_info <- prep_sample_info(args[1])
save_csv(sample_info, args[2], quote=FALSE)