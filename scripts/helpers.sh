
# transform up to $1 characters of the md5sum of stdin into A/C/T/G, lumping
# four values per nuclotide to handle all 16 possible cases.
function md5_nucl {
	md5sum | \
		cut -f 1 -d ' ' | \
		tr '[cdef]' G | \
		tr '[89ab]' T | \
		tr '[4567]' C | \
		tr '[0123]' A | \
		cut -c 1-$1
}

function fake_index {
	awk -v bcode=$1 -v qual=$2 '{if ((NR-1)%4 == 1) {print bcode} else if ( (NR-1)%4 == 3) { print qual} else { print $0} }'
}

function fake_index_main {
	fp=$1
	len=${2-8}
	bcode=$(echo -n $(basename "$fp") | md5_nucl $len)
	qual=$(echo "$bcode" | sed s/./I/g)
	zcat "$fp" | fake_index $bcode $qual | gzip
}
