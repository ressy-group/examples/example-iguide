load_csv <- function(fp, ...) load_table(fp, sep = ",")
load_tsv <- function(fp, ...) load_table(fp, sep = "\t")
save_csv <- function(data, fp, ...) save_table(data, fp, sep = ",", ...)
save_tsv <- function(data, fp, ...) save_table(data, fp, sep = "\t", ...)

load_table <- function(fp, ...) {
  read.table(file = fp, header = TRUE, row.names = NULL, stringsAsFactors = FALSE, ...)
}

save_table <- function(data, fp, ...) {
  write.table(x = data, file = fp, row.names = FALSE, ...)
}

barcode_peek <- function(fp) {
  con <- file(fp, "r")
  data <- strsplit(readChar(con, 1024), "\n")[[1]][2]
  close(con)
  data
}

prep_sample_info <- function(sra_run_table_fp="SraRunTable.tsv") {
  # values for positive and negative replicates are given in separate columns. 
  # We'll collapse each pair of columns for those cases down to a single column.
  sra_run_table <- load_tsv(sra_run_table_fp)
  with(sra_run_table, {
    data.frame(
      sampleName = Library_Name,
      gRNA = gRNA,
      barcode1 = sapply(file.path("raw", paste0(sra_run_table$Run, "_I1.fastq.gz")), barcode_peek),
      barcode2 = sapply(file.path("raw", paste0(sra_run_table$Run, "_I2.fastq.gz")), barcode_peek),
      R1Over = ifelse(grepl("-pos-", Library_Name),
                      overtrim_pos_R1,
                      overtrim_neg_R1),
      R2Lead = ifelse(grepl("-pos-", Library_Name),
                      trim_pos_R2,
                      trim_neg_R2),
      R2LeadODN = ifelse(grepl("-pos-", Library_Name),
                         trimODN_pos,
                         trimODN_neg),
      stringsAsFactors = FALSE)
  })
}

prep_supp_info <- function(sra_run_table_fp="SraRunTable.tsv") {
  sra_run_table <- load_tsv(sra_run_table_fp)
  with(sra_run_table, {
    unique(data.frame(
      specimen = Sample_Name,
      gRNA = gRNA,
      stringsAsFactors = FALSE))
  })
}